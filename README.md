# sparks-of-hope-pipes

Todo:

- [x] Editor button
- [x] Spawn on splines
- [ ] Toggle deform shape on spline by a button in the UI
- [x] 3D Model of the pipe head/corner/butt/tunnel (must fit 1x1x1 meter)
- [ ] Custom icons on buttons
- [ ] Asset validator for length and angle (must be 90 degrees)
- [ ] Auto rotate pipe blocks at angles

// Copyright Epic Games, Inc. All Rights Reserved.

#include "SparksOfHopePipes.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SparksOfHopePipes, "SparksOfHopePipes" );

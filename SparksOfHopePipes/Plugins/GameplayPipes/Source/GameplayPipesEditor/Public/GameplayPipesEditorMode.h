// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Tools/UEdMode.h"

#include "GameplayPipesEditorMode.generated.h"

UCLASS()
class UGameplayPipesEditorMode : public UEdMode
{
	GENERATED_BODY()

public:
	const static FEditorModeID EM_GameplayPipesEditorModeId;

	static FString SpawnToolName;

	UGameplayPipesEditorMode();
	virtual ~UGameplayPipesEditorMode();

	/** UEdMode interface */
	virtual void Enter() override;
	virtual void CreateToolkit() override;
	virtual TMap<FName, TArray<TSharedPtr<FUICommandInfo>>> GetModeCommands() const override;
};

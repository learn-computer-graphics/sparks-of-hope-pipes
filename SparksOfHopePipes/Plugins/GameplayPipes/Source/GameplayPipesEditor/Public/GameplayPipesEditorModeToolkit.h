// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayPipesEditorMode.h"
#include "Toolkits/BaseToolkit.h"

/**
 * This FModeToolkit just creates a basic UI panel that allows various InteractiveTools to
 * be initialized, and a DetailsView used to show properties of the active Tool.
 */
class FGameplayPipesEditorModeToolkit : public FModeToolkit
{
public:
	FGameplayPipesEditorModeToolkit();

	// FModeToolkit interface
	virtual void Init(const TSharedPtr<IToolkitHost>& InitToolkitHost, TWeakObjectPtr<UEdMode> InOwningMode) override;
	virtual void GetToolPaletteNames(TArray<FName>& PaletteNames) const override;
	virtual FText GetActiveToolDisplayName() const override
	{
		return ActiveToolName;
	}

	// IToolkit interface
	virtual FName GetToolkitFName() const override;
	virtual FText GetBaseToolkitName() const override;

protected:
	// FModeToolkit interface
	virtual void OnToolStarted(UInteractiveToolManager* Manager, UInteractiveTool* Tool) override;
	virtual void OnToolEnded(UInteractiveToolManager* Manager, UInteractiveTool* Tool) override;

private:
	TSharedPtr<SWidget> ToolShutdownViewportOverlayWidget;
	void MakeToolShutdownOverlayWidget();

	FText ActiveToolName;
};

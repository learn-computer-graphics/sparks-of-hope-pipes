// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class GameplayPipesEditor : ModuleRules
{
    public GameplayPipesEditor(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(
            new string[]
            {
                "Core",
                "SlateCore",
            }
        );

        PrivateDependencyModuleNames.AddRange(
            new string[]
            {
                "CoreUObject",
                "DataValidation",
                "EditorFramework",
                "EditorInteractiveToolsFramework",
                "EditorStyle",
                "Engine",
                "GameplayPipes",
                "InputCore",
                "InteractiveToolsFramework",
                "LevelEditor",
                "Slate",
                "ToolWidgets",
                "UnrealEd",
            }
        );
    }
}

// Copyright Epic Games, Inc. All Rights Reserved.

#include "GameplayPipesEditorModule.h"

#include "GameplayPipesEditorModeCommands.h"

#define LOCTEXT_NAMESPACE "GameplayPipesEditorModule"

void FGameplayPipesEditorModule::StartupModule()
{
	FGameplayPipesEditorModeCommands::Register();
}

void FGameplayPipesEditorModule::ShutdownModule()
{
	FGameplayPipesEditorModeCommands::Unregister();
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FGameplayPipesEditorModule, GameplayPipesEditorMode)

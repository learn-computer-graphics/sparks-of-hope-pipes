// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EditorValidatorBase.h"

#include "GameplayPipes_LevelValidator.generated.h"

/**
 *
 */
UCLASS()
class UGameplayPipes_LevelValidator : public UEditorValidatorBase
{
	GENERATED_BODY()

protected:
	virtual bool CanValidateAsset_Implementation(UObject* InAsset) const override;
	virtual EDataValidationResult ValidateLoadedAsset_Implementation(UObject* InAsset, TArray<FText>& ValidationErrors) override;

private:
	EDataValidationResult ValidatePipe(class AGameplayPipes_PipeActor& Pipe, TArray<FText>& ValidationErrors) const;
};

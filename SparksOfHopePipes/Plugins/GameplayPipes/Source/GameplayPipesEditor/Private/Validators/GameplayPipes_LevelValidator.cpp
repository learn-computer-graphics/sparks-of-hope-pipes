// Fill out your copyright notice in the Description page of Project Settings.

#include "Validators/GameplayPipes_LevelValidator.h"

#include "Engine/Level.h"
#include "Engine/World.h"
#include "GameplayPipes_PipeActor.h"

bool UGameplayPipes_LevelValidator::CanValidateAsset_Implementation(UObject* InAsset) const
{
	return (InAsset != nullptr) && (UWorld::StaticClass() == InAsset->GetClass());
}

EDataValidationResult UGameplayPipes_LevelValidator::ValidateLoadedAsset_Implementation(
	UObject* InAsset, TArray<FText>& ValidationErrors)
{
	EDataValidationResult Result = EDataValidationResult::Valid;
	UWorld* World = CastChecked<UWorld>(InAsset);
	for (const ULevel* Level : World->GetLevels())
	{
		for (AActor* Actor : Level->Actors)
		{
			if (AGameplayPipes_PipeActor* Pipe = Cast<AGameplayPipes_PipeActor>(Actor))
			{
				if (ValidatePipe(*Pipe, ValidationErrors) != EDataValidationResult::Valid)
				{
					Result = EDataValidationResult::Invalid;
				}
			}
		}
	}

	if (Result == EDataValidationResult::Invalid)
	{
		AssetFails(InAsset, FText::FromString("Level has an invalid PipeActor"), ValidationErrors);
	}
	else
	{
		AssetPasses(InAsset);
	}

	return Result;
}

EDataValidationResult UGameplayPipes_LevelValidator::ValidatePipe(
	AGameplayPipes_PipeActor& Pipe, TArray<FText>& ValidationErrors) const
{
	return EDataValidationResult::Invalid;
}

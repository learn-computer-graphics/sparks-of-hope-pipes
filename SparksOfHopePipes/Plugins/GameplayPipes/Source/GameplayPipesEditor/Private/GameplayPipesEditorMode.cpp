// Copyright Epic Games, Inc. All Rights Reserved.

#include "GameplayPipesEditorMode.h"

#include "EdModeInteractiveToolsContext.h"
#include "GameplayPipesEditorModeCommands.h"
#include "GameplayPipesEditorModeToolkit.h"
#include "InteractiveToolManager.h"
#include "Tools/GameplayPipes_SpawnTool.h"

#define LOCTEXT_NAMESPACE "GameplayPipesEditorMode"

const FEditorModeID UGameplayPipesEditorMode::EM_GameplayPipesEditorModeId = TEXT("EM_GameplayPipesEditorMode");

FString UGameplayPipesEditorMode::SpawnToolName = TEXT("GameplayPipesEditor_SpawnTool");

UGameplayPipesEditorMode::UGameplayPipesEditorMode()
{
	FModuleManager::Get().LoadModule("EditorStyle");

	// #GH_TODO add a new icon and register it in the theme to be able to use it
	// appearance and icon in the editing mode ribbon can be customized here
	Info =
		FEditorModeInfo(UGameplayPipesEditorMode::EM_GameplayPipesEditorModeId, LOCTEXT("ModeName", "Pipes"), FSlateIcon(), true);
}

UGameplayPipesEditorMode::~UGameplayPipesEditorMode()
{
}

void UGameplayPipesEditorMode::Enter()
{
	UEdMode::Enter();

	const FGameplayPipesEditorModeCommands& PipeCommands = FGameplayPipesEditorModeCommands::Get();

	RegisterTool(PipeCommands.SpawnTool, SpawnToolName, NewObject<UGameplayPipes_SpawnToolBuilder>(this));

	// active tool type is not relevant here, we just set to default
	GetToolManager()->SelectActiveToolType(EToolSide::Left, SpawnToolName);
}

void UGameplayPipesEditorMode::CreateToolkit()
{
	Toolkit = MakeShareable(new FGameplayPipesEditorModeToolkit);
}

TMap<FName, TArray<TSharedPtr<FUICommandInfo>>> UGameplayPipesEditorMode::GetModeCommands() const
{
	return FGameplayPipesEditorModeCommands::Get().GetCommands();
}

#undef LOCTEXT_NAMESPACE

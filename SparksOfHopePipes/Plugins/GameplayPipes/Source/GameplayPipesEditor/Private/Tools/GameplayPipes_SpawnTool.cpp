// Copyright Epic Games, Inc. All Rights Reserved.

#include "GameplayPipes_SpawnTool.h"

#include "BaseBehaviors/MouseHoverBehavior.h"
#include "Components/SplineComponent.h"
#include "Engine/World.h"
#include "GameplayPipes_PipeActor.h"
#include "InteractiveToolManager.h"
#include "SceneManagement.h"
#include "ToolBuilderUtil.h"

// localization namespace
#define LOCTEXT_NAMESPACE "UGameplayPipesEditorInteractiveTool"

UInteractiveTool* UGameplayPipes_SpawnToolBuilder::BuildTool(const FToolBuilderState& SceneState) const
{
	UGameplayPipes_SpawnTool* NewTool = NewObject<UGameplayPipes_SpawnTool>(SceneState.ToolManager);
	NewTool->SetWorld(SceneState.World);
	return NewTool;
}

UGameplayPipes_SpawnTool::UGameplayPipes_SpawnTool(const FObjectInitializer& ObjectInitializer)
{
	UInteractiveTool::SetToolDisplayName(LOCTEXT("SpawnToolName", "Create Pipe"));
}

void UGameplayPipes_SpawnTool::SetWorld(UWorld* World)
{
	check(World);
	TargetWorld = World;
}

void UGameplayPipes_SpawnTool::Setup()
{
	Super::Setup();

	Properties = NewObject<UGameplayPipes_SpawnToolProperties>(this, "Settings");
	AddToolPropertySource(Properties);
}

void UGameplayPipes_SpawnTool::Shutdown(EToolShutdownType ShutdownType)
{
	if (SelectedPipe && ShutdownType == EToolShutdownType::Cancel)
	{
		SelectedPipe->Destroy();
	}

	SelectedPipe = nullptr;
}

bool UGameplayPipes_SpawnTool::CanAccept() const
{
	return CurrentState == EState::AddControlPoint;
}

void UGameplayPipes_SpawnTool::OnClicked(const FInputDeviceRay& ClickPos)
{
	switch (CurrentState)
	{
		case EState::SpawnPipe:
			if (SpawnPipeAtPosition(ClickPos))
			{
				CurrentState = EState::AddControlPoint;
			}
			break;

		case EState::AddControlPoint:
			AddControlPointAtPosition(ClickPos);
			break;

		default:
			checkNoEntry();
			break;
	}
}

FInputRayHit UGameplayPipes_SpawnTool::IsHitByClick(const FInputDeviceRay& ClickPos)
{
	FInputRayHit Result(0);
	Result.bHit = true;
	return Result;
}

bool UGameplayPipes_SpawnTool::SpawnPipeAtPosition(const FInputDeviceRay& ClickPos)
{
	if (!TargetWorld || Properties->ActorTypeToSpawn.IsNull())
	{
		return false;
	}

	const FVector SpawnPos = FindSpawnPosition(ClickPos);

	GetToolManager()->BeginUndoTransaction(LOCTEXT("AddPipeToolTransactionName", "Add Pipe"));
	SelectedPipe = (AGameplayPipes_PipeActor*) TargetWorld->SpawnActor(Properties->ActorTypeToSpawn.Get(), &SpawnPos);
	GetToolManager()->EndUndoTransaction();

	return true;
}

void UGameplayPipes_SpawnTool::AddControlPointAtPosition(const FInputDeviceRay& ClickPos)
{
	if (!SelectedPipe)
	{
		return;
	}

	const FVector SpawnPos = FindSpawnPosition(ClickPos);
	USplineComponent* Spline = SelectedPipe->GetComponentByClass<USplineComponent>();
	Spline->AddSplinePoint(SpawnPos, ESplineCoordinateSpace::World);

	SelectedPipe->RerunConstructionScripts();
}

FVector UGameplayPipes_SpawnTool::FindSpawnPosition(const FInputDeviceRay& ClickPos) const
{
	// #GH_TODO take mesh to spawn bounds into account so that it does not traverse the ground
	// #GH_TODO handle if ground z is higher than 0

	const FRay& ClickPosWorldRay = ClickPos.WorldRay;
	const FPlane DrawPlane(FVector::ZeroVector, FVector(0, 0, 1));
	const FVector SpawnPos = FMath::RayPlaneIntersection(ClickPosWorldRay.Origin, ClickPosWorldRay.Direction, DrawPlane);
	return SpawnPos;
}

#undef LOCTEXT_NAMESPACE

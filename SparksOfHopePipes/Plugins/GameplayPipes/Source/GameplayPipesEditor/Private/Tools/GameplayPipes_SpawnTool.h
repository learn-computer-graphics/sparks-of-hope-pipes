// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "BaseTools/SingleClickTool.h"
#include "CoreMinimal.h"
#include "InteractiveToolBuilder.h"
#include "InteractiveToolQueryInterfaces.h"

#include "GameplayPipes_SpawnTool.generated.h"

UCLASS()
class GAMEPLAYPIPESEDITOR_API UGameplayPipes_SpawnToolBuilder : public UInteractiveToolBuilder
{
	GENERATED_BODY()

public:
	virtual bool CanBuildTool(const FToolBuilderState& SceneState) const override
	{
		return true;
	}
	virtual UInteractiveTool* BuildTool(const FToolBuilderState& SceneState) const override;
};

UCLASS(Transient)
class GAMEPLAYPIPESEDITOR_API UGameplayPipes_SpawnToolProperties : public UInteractiveToolPropertySet
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category = Options)
	TSoftClassPtr<class AGameplayPipes_PipeActor> ActorTypeToSpawn;
};

UCLASS()
class GAMEPLAYPIPESEDITOR_API UGameplayPipes_SpawnTool : public USingleClickTool
{
	GENERATED_BODY()

public:
	UGameplayPipes_SpawnTool(const FObjectInitializer& ObjectInitializer);

	void SetWorld(UWorld* World);

	// Begin UInteractiveTool
	virtual void Setup() override;
	virtual void Shutdown(EToolShutdownType ShutdownType) override;
	virtual bool HasCancel() const override
	{
		return true;
	}
	virtual bool HasAccept() const override
	{
		return true;
	}
	virtual bool CanAccept() const override;
	// End UInteractiveTool

	// #GH_TODO check FLandscapeToolSplines::AddControlPoint to handle spline creation directly via the tool

	// Begin USingleClickTool
	virtual void OnClicked(const FInputDeviceRay& ClickPos) override;
	virtual FInputRayHit IsHitByClick(const FInputDeviceRay& ClickPos) override;
	// End USingleClickTool

protected:
	enum class EState
	{
		SpawnPipe,
		AddControlPoint
	};

	EState CurrentState = EState::SpawnPipe;
	UWorld* TargetWorld = nullptr;

	UPROPERTY()
	TObjectPtr<UGameplayPipes_SpawnToolProperties> Properties;

	UPROPERTY()
	TObjectPtr<class AGameplayPipes_PipeActor> SelectedPipe;

private:
	bool SpawnPipeAtPosition(const FInputDeviceRay& ClickPos);
	void AddControlPointAtPosition(const FInputDeviceRay& ClickPos);

	FVector FindSpawnPosition(const FInputDeviceRay& ClickPos) const;
};

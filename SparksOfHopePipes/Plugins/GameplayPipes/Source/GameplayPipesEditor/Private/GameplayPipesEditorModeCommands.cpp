// Copyright Epic Games, Inc. All Rights Reserved.

#include "GameplayPipesEditorModeCommands.h"

#include "EditorStyleSet.h"
#include "GameplayPipesEditorMode.h"

#define LOCTEXT_NAMESPACE "GameplayPipesEditorModeCommands"

FGameplayPipesEditorModeCommands::FGameplayPipesEditorModeCommands()
	: TCommands<FGameplayPipesEditorModeCommands>("GameplayPipesEditorMode",
		  NSLOCTEXT("GameplayPipesEditorMode", "GameplayPipesEditorModeCommands", "GameplayPipesEditor Editor Mode"), NAME_None,
		  FAppStyle::GetAppStyleSetName())
{
}

void FGameplayPipesEditorModeCommands::RegisterCommands()
{
	TArray<TSharedPtr<FUICommandInfo>>& ToolCommands = Commands.FindOrAdd(NAME_Default);

	UI_COMMAND(SpawnTool, "New", "Spawns a new pipe on click", EUserInterfaceActionType::Button, FInputChord());
	ToolCommands.Add(SpawnTool);
}

TMap<FName, TArray<TSharedPtr<FUICommandInfo>>> FGameplayPipesEditorModeCommands::GetCommands()
{
	return FGameplayPipesEditorModeCommands::Get().Commands;
}

#undef LOCTEXT_NAMESPACE

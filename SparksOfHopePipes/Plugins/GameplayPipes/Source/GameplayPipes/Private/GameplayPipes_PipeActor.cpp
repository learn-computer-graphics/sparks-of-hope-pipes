// Fill out your copyright notice in the Description page of Project Settings.

#include "GameplayPipes_PipeActor.h"

#include "Components/SplineComponent.h"
#include "Components/SplineMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Misc/MapErrors.h"
#include "Misc/UObjectToken.h"

AGameplayPipes_PipeActor::AGameplayPipes_PipeActor(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	SplineComponent = CreateDefaultSubobject<USplineComponent>(TEXT("SplineComponent"));
	SplineComponent->Mobility = EComponentMobility::Static;
	SplineComponent->SetGenerateOverlapEvents(false);

	RootComponent = SplineComponent;
}

void AGameplayPipes_PipeActor::OnConstruction(const FTransform& Transform)
{
	const int32 SplinePointCount = SplineComponent->GetNumberOfSplinePoints();

	// #GH_TODO abstract all of this

	// Head
	if (SplinePointCount > 1)
	{
		UStaticMeshComponent* MeshComponent =
			(UStaticMeshComponent*) AddComponentByClass(UStaticMeshComponent::StaticClass(), false, FTransform::Identity, false);
		MeshComponent->SetStaticMesh(PipeHeadMesh);

		FVector StartLocation;
		FVector StartTangent;
		SplineComponent->GetLocationAndTangentAtSplinePoint(0, StartLocation, StartTangent, ESplineCoordinateSpace::Local);

		FVector EndLocation;
		FVector EndTangent;
		SplineComponent->GetLocationAndTangentAtSplinePoint(1, EndLocation, EndTangent, ESplineCoordinateSpace::Local);

		const FVector StartToEnd = StartLocation - EndLocation;
		FTransform Transform = FTransform::Identity;
		Transform.SetLocation(StartLocation);
		Transform.SetRotation(StartToEnd.Rotation().Quaternion());

		MeshComponent->SetRelativeTransform(Transform);
	}

	// Butt
	if (SplinePointCount > 3)
	{
		UStaticMeshComponent* MeshComponent =
			(UStaticMeshComponent*) AddComponentByClass(UStaticMeshComponent::StaticClass(), false, FTransform::Identity, false);
		MeshComponent->SetStaticMesh(PipeTailMesh);

		FVector StartLocation;
		FVector StartTangent;
		SplineComponent->GetLocationAndTangentAtSplinePoint(
			SplinePointCount - 1, StartLocation, StartTangent, ESplineCoordinateSpace::Local);

		FVector EndLocation;
		FVector EndTangent;
		SplineComponent->GetLocationAndTangentAtSplinePoint(
			SplinePointCount - 2, EndLocation, EndTangent, ESplineCoordinateSpace::Local);

		const FVector StartToEnd = StartLocation - EndLocation;
		FTransform Transform = FTransform::Identity;
		Transform.SetLocation(StartLocation);
		Transform.SetRotation(StartToEnd.Rotation().Quaternion());

		MeshComponent->SetRelativeTransform(Transform);
	}

	// Body
	const int32 SplineIndexBeforeTail = SplinePointCount - 1;
	for (int32 i = 0; i < SplineIndexBeforeTail; i++)
	{
		// #GH_TODO if angle is near 90 degrees, use the pipe corner mesh and rotate it properly

		USplineMeshComponent* SplineMeshComponent =
			(USplineMeshComponent*) AddComponentByClass(USplineMeshComponent::StaticClass(), false, FTransform::Identity, false);

		SplineMeshComponent->SetStaticMesh(PipeBodyMesh);

		FVector StartLocation;
		FVector StartTangent;
		SplineComponent->GetLocationAndTangentAtSplinePoint(i, StartLocation, StartTangent, ESplineCoordinateSpace::Local);

		FVector EndLocation;
		FVector EndTangent;
		SplineComponent->GetLocationAndTangentAtSplinePoint(i + 1, EndLocation, EndTangent, ESplineCoordinateSpace::Local);

		SplineMeshComponent->SetStartAndEnd(StartLocation, StartTangent, EndLocation, EndTangent, true);
	}
}

void AGameplayPipes_PipeActor::CheckForErrors()
{
	Super::CheckForErrors();

	FMessageLog MapCheck("MapCheck");

	if (!PipeBodyMesh || !PipeCornerMesh || !PipeHeadMesh || !PipeTailMesh)
	{
		MapCheck.Warning()
			->AddToken(FUObjectToken::Create(this))
			->AddToken(FTextToken::Create(FText::FromString("Pipe actor has NULL PipeMesh property")))
			->AddToken(FMapErrorToken::Create(FMapErrors::StaticMeshNull));
	}
}

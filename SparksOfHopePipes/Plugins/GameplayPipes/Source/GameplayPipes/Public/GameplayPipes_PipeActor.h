// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "GameplayPipes_PipeActor.generated.h"

/**
 *
 */
UCLASS(abstract)
class GAMEPLAYPIPES_API AGameplayPipes_PipeActor : public AActor
{
	GENERATED_BODY()

public:
	AGameplayPipes_PipeActor(const FObjectInitializer& ObjectInitializer);

	// Begin AActor
	virtual void OnConstruction(const FTransform& Transform) override;

#if WITH_EDITOR
	virtual void CheckForErrors() override;
#endif	  // WITH_EDITOR

	// End AActor

private:
	UPROPERTY(Category = SplineActor, VisibleAnywhere, BlueprintReadOnly,
		meta = (ExposeFunctionCategories = "Mesh,Rendering,Physics,Components|StaticMesh,Components|SplineMesh",
			AllowPrivateAccess = "true"))
	TObjectPtr<class USplineComponent> SplineComponent;

	UPROPERTY(Category = SplineActor, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UStaticMesh> PipeBodyMesh;

	UPROPERTY(Category = SplineActor, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UStaticMesh> PipeHeadMesh;

	UPROPERTY(Category = SplineActor, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UStaticMesh> PipeTailMesh;

	UPROPERTY(Category = SplineActor, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
	TObjectPtr<class UStaticMesh> PipeCornerMesh;
};
